import React from 'react'

export default (props: { children: React.ReactNode }) => (
  <main>
     {props.children}
  </main>
)